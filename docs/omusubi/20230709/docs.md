# 2023夏・PCの選び方

![Alt text](<how to buy gaming pc.png>)

ゲーマー、AIで遊びたい人向けのPCの選び方について説明します。

??? "見るべきポイント"
    - 値段
        - 10~20万円
        - 18万、5年 → 月額3000円
        - 【参考】PCのサブスク、月額7000円、[Lenovoスグゲー](https://legion-sugu-game.com/)
        - [価格コムPC](https://kakaku.com/pc/desktop-pc/)
    - CPU
        - intel/AMD(Ryzen)
        - あまり重視しない、intel/AMDは好み (どちらかというとintel)
        - 目安: i5, Ryzen5以上
        - [パソコン選び方ガイド/CPU](https://pcrecommend.com/cpu/)
    - メモリ
        - 目安: 16GB以上
    - ストレージ
        - M.2 SSD
        - 昔よりだいぶ安い
        - 目安: 1TB以上
    - GPU
        - NVIDIA(GeForce)/AMD(Radeon)
        - 基本的にGeForce
        - 目安: 3060, 3060Ti, 3070, 4060, 4060Tiあたり
        - VRAM: AIで遊びたいなら超重要
        - 目安: 10GB以上、ゲームならそんなに気にしない
        - [パソコン選び方ガイド/GPU](https://pcrecommend.com/gpu/)
    - 電源
        - 目安: 700W
        - そんなに気にしない (小さいとPCが壊れる、大きいと電気代がかかる)
        - [電源計算機](https://www.dospara.co.jp/5info/cts_str_power_calculation_main.html)
    - 見た目
    ??? Abstract "画像"
        - ![Alt text](image-3.png)
        - ![Alt text](image-4.png)
        - ![Alt text](image-5.png)
        - ![Alt text](image-6.png)
    ??? Tip "各パーツの役割"
        - メモリは机
        - ストレージは引き出し
        - CPU/GPUは脳
            - CPUは「普通」の計算
            - GPUは行列演算、ゲーム、動画作成など
            - GPUでの演算に対応しているものが増えている。
                - GPGPU (Nintendo Switch)
                - Chromeのハードウェア・アクセラレーション

??? とりあえず価格コムで調べよう
    - おすすめはGPUをベースに調べる
    - [価格コムのPCのページ](https://kakaku.com/pc/desktop-pc/)
    !!! GPUを絞る
        - ![Alt text](image-7.png)
    !!! おすすめはこの辺
        - ![Alt text](image-8.png)
    !!! こだわりがなければ安い順
        - ![Alt text](image-9.png)

??? デスクトップPCかノートPCか
    - 基本的にはデスクトップがおすすめ
    - デスクトップPCの問題: ディスプレイ/キーボード/マウス/置く場所が必要
    - ノートPCの問題
        - 選ぶポイントが増える
            - ディスプレイの見え方/キーボードやタッチパッド/熱のこもり方/ファンのうるささ/重量…
            - 一体型なので一つでも気に入らない点があると微妙
        - 一回り性能の高いものを買う必要がある
            - ノートPC向けのGPUはデスクトップ向けよりちょっと性能が低い
            - (外付けGPUはまだマニア向け)
            - パーツの交換が容易でない

??? "ハチアンのPC"
    - mouseで購入
        - 同じのはないけど[これに近い](https://www.mouse-jp.co.jp/store/g/ggtune-dgi5g60b7acbw101dec/)
    - 価格: 14万
    - CPU: i5-11400F
    - メモリ: 16GB
    - ストレージ: 1TB
    - GPU: RTX3060, 12GB, 170W
    - 電源: 700W (BRONZE)
    ??? Tip "ハチアンのPC遍歴"
        - ~2005年: 家用PC
        - ~2010年: Gateway
        - 2010年: GTX460のPC (13万)
        - 2015年くらい？: GTX750Tiに換装
        - 2018年: GTX1060に換装
        - 2019年: GTX1660TiのPC (11万)　
        - 2022年: RTX3060のPC (14万)

??? "今買うならこれ"
    - **本当はRTX4060Ti, 16GBが出るまで待ちたい**
    - どうしても今すぐなら[これ](https://www.dospara.co.jp/TC30/MC12695.html?kc=1&_bdadid=JPGTE5.0000dc99l)か[これ](https://www.dospara.co.jp/TC30/MC12503.html?kc=1&_bdadid=JPGTE5.0000dc99l)
    ??? Abstract "候補1"
        - ![Alt text](image-1.png)
        - 4060Tiは115W
    ??? Abstract "候補2"
        - ![Alt text](image.png)
        - 4070は200W
    - CPU/電源がちょっと弱い
        - ![Alt text](image-2.png)
        - 電源650W (BRONZE) ← ちょっと不安

??? info "参考文献"
    - [Lenovoスグゲー](https://legion-sugu-game.com/)
    - [価格コムPC](https://kakaku.com/pc/desktop-pc/)
    - [パソコン選び方ガイド/CPU](https://pcrecommend.com/cpu/)
    - [パソコン選び方ガイド/GPU](https://pcrecommend.com/gpu/)
    - [電源計算機](https://www.dospara.co.jp/5info/cts_str_power_calculation_main.html)
    - [木材を使ったPC](https://pc-seven.co.jp/spc/19966.html)
    - [Alienware](https://www.dell.com/ja-jp/shop/%E8%A3%BD%E5%93%81%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA/sf/alienware-desktops)
    - [PREDATOR](https://www.acer.com/jp-ja/predator/desktops-and-all-in-ones/predator-orion)
    - [Mac Pro](https://www.apple.com/jp/shop/buy-mac/mac-pro)