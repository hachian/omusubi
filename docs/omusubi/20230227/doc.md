# Pythonのデータ構造

??? "リスト関連の関数"
    - `append`: 末尾に要素を付け加える
    - `insert`: 指定した位置に要素を挿入する
    - `remove`: 指定した要素を削除
    - `pop`: 末尾の要素を削除し、取り出す
    ??? example
        ```python
        x = [1, 2, 3]
        y = x
        print(f"x_original = {x}")
        print(f"y_original = {y}")
        x.append(4)  # 「4」を末尾につける
        print(f"x_append = {x}")
        print(f"y_append = {y}")
        x.insert(0, -1)  # 0番目に「-1」を挿入
        print(f"x_insert = {x}")
        x.remove(3)  # 要素「3」を削除
        print(f"x_remove = {x}")
        popped = x.pop(1)  # 1番目を削除、poppedに入れる
        print(f"x_pop = {x}")
        print(f"popped = {popped}")
        ```
    - `clear`: リストを削除
    - `index`: 要素を見つけてそのインデックスを返す
    - `count`: 出現回数を返す
    - `sort`: ソートする(inplace)
    - `reverse`: 逆順にする(inplace)
    ??? example
        ```python
        z = ["a", "a", "b", "b", "b", "c", "c", "c", "c"]
        print(f"z_index = {z.index('c')}")  # 最初にcが出てくるインデックス
        print(f"z_index = {z.index('a', 2)}")  # 2以降でaが出てくるインデックス、ないのでエラー
        print(f"z_count = {z.count('b')}")  # bの個数

        w = [7, 2, 3, 1]
        ret = w.sort()  # ソート、返り値は無しに注意
        print(f"w_sort = {w}")
        print(f"ret = {ret}")
        ret = sorted(w)  # これは返ってくる
        print(f"ret = {ret}")
        ret = w.reverse()  # 逆順
        print(f"w_reverse = {w}")
        print(f"ret = {ret}")  # 返ってこない
        ```
    - `copy`: 浅いコピーを作る
    ??? example
        ```python
        a = [1, 2, 3]
        b = a
        c = a.copy()
        a.append(4)
        print(a)
        print(b)
        print(c)
        ```
    ??? Question "「浅い」コピーとは"
        ```python
        d = [1, 2, [3, 4]]
        e = d.copy()
        d.append(1)
        print(d)
        print(e)
        d[2].append(5)
        print(d)
        print(e)
        ```

??? "リストの内包表記"
    - 一行でリストが作れる
    - 例えば以下
        - `[i**2 for i in range(10)]`
    - ネストすることができる
        - `[[i + j * 3 for i in range(3)] for j in range(4)]`
    - 後ろからif
        - `[i for i in range(1, 20) if i % 3 == 0]`
    - 三項演算子
        - `["fizz" if i % 3 == 0 else i for i in range(1, 20)]`
    ??? tip
        - 後ろからifのパターン：ループ回数と要素数が合わない
        - 三項演算子のパターン：ループ回数と要素数が同じ

??? "clearとdel"
    - `clear`: リストを削除(からのリストにする)
    - `del`: 変数自体削除する
    ```python
    x = [0, 0, 0]
    x.clear()
    print(x)  # xは空のリスト(Noneではない)
    del x  # xにはもうアクセスできない
    ```
    - `del`でインデックス番目の要素を消すことができる
    ```python
    x = [1, 2, 3, 4, 5, 6, 7, 8]
    del x[1]
    print(x)
    del x[2::2]
    print(x)
    ```

??? "タプル"
    - `()`で囲むとタプルになる
        - 実は`()`無しでもOK
        - `tuple()`でもOK
    - `list`との違い
        - mutableかimutableか
        - タプルは中身を変えたりできない
    - 分割して変数への代入ができる
        - `a, b = (1, 2)`
    ??? example
        ```python
        t = (1, 2, 3)
        print(t)
        # t[2] = 4  # エラーになる
        tt = 1, 2, 3  # ()無しでもOK
        print(tt)
        a, b, c = 1, 2, 3  # a, b, cそれぞれに代入
        print(f"a = {a}, b = {b}, c = {c}")
        a, b = b, a  # 交換できる
        print(f"a = {a}, b = {b}, c = {c}")
        ```

??? "集合"
    - `{}`で囲むと集合になる
        - `set()`でもOK
    - inで集合の要素か判定できる
    ??? example
        ```python
        gu = {"shake", "okaka", "kombu", "tsunamayo"}
        print(gu)
        print("kombu" in gu)
        print("umeboshi" in gu)
        ```
    - `set`の要素は重複を許さない
    - `-`, `|`, `&`, `^`などの演算が使える
    ??? example
        ```python
        a = set("omusubi")  # (1)文字の集合になる
        b = set("tsunamayo")  # (1)文字の集合になる
        print(a)  # 重複はない
        print(b)  # 重複はない
        print(a - b)  # 差集合
        print(a | b)  # 和集合
        print(a & b)  # 共通集合
        print(a ^ b)  # 対象差集合
        ```
    - `a[1]`などにアクセスはできない

??? "辞書"
    - `d = {key1: value1, key2: value2, key3: value3}`の形で記載
        - `dict(key1=value1, key2=vlaue2, key3=value3)`でもOK
    - `key`にできるものはimmutableなもの
    - `in`で存在するキーか判定できる
    - `d["key1"]`でアクセスできる
    ??? example
        ```python
        d = {
            "hoge": 1,
            "fuga": 2,
            "piyo": 3
        }
        print(d)
        print("hoge" in d)
        print(d["fuga"])
        ```
    - ループの書き方
        - keysでキーが取得できる
        - valuesで値が取得できる
        - itemsで両方取得できる
    ??? example
        ```python
        d = {
            "hoge": 1,
            "fuga": 2,
            "piyo": 3
        }
        for k in d.keys():
            print(k)
        for v in d.values():
            print(v)
        for k, v in d.items():
            print(f"{k}: {v}")
        ```

??? info "参考文献"
    - https://docs.python.org/ja/3/tutorial/datastructures.html
