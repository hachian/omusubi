# 配信資料のトップページ

左から配信資料へアクセス

## テンプレート

```` markdown
# 題名

??? "見出し1"
    - `socs/omusubi/{YYYYMMDD}/doc.md`として保存
    - item1
    - item2
    ??? Example "例です。"
        ```python
        a = "an"
        print(f"this is {a} example code")
        ```
    ??? Question "詳細な内容です。"
        - item1

??? info "参考文献"
    - https://ch-omusubi.com/home
````
- ![](Admonitions.png)
