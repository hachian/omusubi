## このページは？

ここはmkdocsの記法テストページです。

`mkdocs serve`でライブプレビューサーバが立ち上がります。[http://127.0.0.1:8000](http://127.0.0.1:8000)にアクセスするとライブプレビューを閲覧することができます。

```powershell
conda activate mkdocs
mkdocs serve
```

## h2h2h2

normal, **bold**, *italic*, `inline`

- list
- list
- list

### h3h3h3

1. list
1. list
1. list

#### h4h4h4

```python
import numpy as np

a = 3
b = "ddd"
print(a)  # comment
# これがコメントだ！
```

!!! note
    note here

??? tip "Admonitionsの種類"
    !!! note
    !!! abstract
    !!! info
    !!! tip
    !!! success
    !!! question
    !!! warning
    !!! failure
    !!! danger
    !!! bug
    !!! example
    !!! quote
