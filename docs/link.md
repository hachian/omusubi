
当サイト: [https://hachian.gitlab.io/omusubi/](https://hachian.gitlab.io/omusubi/)

## [おむすびチャンネル](https://ch-omusubi.com/home)

!!! tip "メインコンテンツ"
    - [ライブ](https://ch-omusubi.com/stream)
    - [アーカイブ](https://ch-omusubi.com/stream/archive)
    - [動画](https://ch-omusubi.com/stream/video)

!!! abstract "サブコンテンツ"
    - [記事](https://ch-omusubi.com/user/community?tab=articles&article-category=all)
    - [グルチャ](https://ch-omusubi.com/user/groupChat)
    - [掲示板](https://ch-omusubi.com/user/forum)
    - [プロフィール](https://ch-omusubi.com/user/profile?id=dXNlciNhdXRoMHw2MmM0MWUyZWM5ZDI5Yzc3ODY2NzVkODk%3D)

!!! Danger "配信者"
    - [ダッシュボード](https://ch-omusubi.com/streamer)
    - [グルチャ](https://ch-omusubi.com/streamer/groupChat)
    - [アーカイブ](https://ch-omusubi.com/streamer/archive)
    - [動画](https://ch-omusubi.com/streamer/video)
    - [プロフィール](https://ch-omusubi.com/user/channel?id=dXNlciNhdXRoMHw2MmM0MWUyZWM5ZDI5Yzc3ODY2NzVkODk%3D&tab=1)
