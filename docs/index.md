# Home

[おむすびチャンネル](https://ch-omusubi.com/)配信用資料の置き場です。

[リンク](https://hachian.gitlab.io/omusubi/)

## コンテンツ

- [おむすび発表資料](omusubi/omusubi-top)
- [便利な直リンク](link)
- [サンプルと記法](sample)
    - Material for MkDocsの記法と本ページのスタイルを確認することができます。
